if [ $1 -ge 0 ] #Runs program if input is greater than or equals 0
then

#define 4 first values
n1=0
n2=0
n3=0
n4=1
curn=$n4
max=$1

#prints first 3 values
echo $n1
echo $n2
echo $n3

#runs while loop as long as input is greater or equals the current tetranacci number
while [ $max -ge $curn ]; do

	#Prints the current number
	echo $curn
	
	#Calculate current tetranacci based upon the other numbers
	curn=$(($n1+$n2+$n3+$n4))
		
	#Recalculates the numbers
	n1=$n2
	n2=$n3
	n3=$n4
	n4=$curn
	done
fi
