if [ $1 -ge 0 ]
then

n1=0
n2=0
n3=0
n4=1
curn=1
max=$1
echo $n1
echo $n2
echo $n3
while [ $max -ge $curn ]; do
	#Prints the current number
	echo $curn
	
	#Calculate based upon the other numbers
	curn=$(($n1+$n2+$n3+$n4))
		
	#Recalculates the numbers
	n1=$n2
	n2=$n3
	n3=$n4
	n4=$curn
	done
fi
